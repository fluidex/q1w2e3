use arrayvec::ArrayVec;
use std::{
    fs::File,
    io::{BufRead, BufReader, Error, ErrorKind},
    time::Instant,
};

const SAFE_NUMBERS_AMOUNT: usize = 100;
// Assuming that the length of a string with `u128::MAX` is 39.
const NUMBER_STRING_CAPACITY: usize = 39;

fn main() -> Result<(), Error> {
    let elapsed = Instant::now();
    let mut number_tunnel = BufReader::new(File::open("challenge_input.txt")?);

    // Circular buffer
    let mut buffer = [0; SAFE_NUMBERS_AMOUNT];
    let mut raw_number = String::with_capacity(NUMBER_STRING_CAPACITY);

    // Buffering the first `SAFE_NUMBERS_AMOUNT` numbers

    buffer.iter_mut().try_for_each(|number| {
        number_tunnel
            .read_line(&mut raw_number)
            .and_then(|bytes_read| {
                // It's expected that the first `SAFE_NUMBERS_AMOUNT` numbers
                // will always be present.
                if bytes_read == 0 {
                    Err(ErrorKind::UnexpectedEof.into())
                } else {
                    Ok(bytes_read)
                }
            })
            .and_then(|bytes_read| match parser(&mut raw_number, bytes_read) {
                Ok(parsed_number) => {
                    *number = parsed_number;

                    Ok(())
                }
                Err(error) => Err(error),
            })
    })?;

    let mut cursor = 0;
    let mut line = SAFE_NUMBERS_AMOUNT;
    let mut sorted_buffer = buffer;

    loop {
        let bytes_read = number_tunnel.read_line(&mut raw_number)?;

        if bytes_read == 0 {
            println!("The end of the tunnel!");

            return Ok(());
        }

        let parsed_number = parser(&mut raw_number, bytes_read)?;

        sorted_buffer.sort();

        let result = analyze(sorted_buffer.iter(), parsed_number);

        if let Some(number_for_crumble) = result {
            println!("{number_for_crumble} on the {} line.", line + 1);
            println!("Done in {:?}.", elapsed.elapsed());

            return Ok(());
        }

        sorted_buffer[sorted_buffer.binary_search(&buffer[cursor]).unwrap()] = parsed_number;
        buffer[cursor] = parsed_number;

        cursor = (cursor + 1) % SAFE_NUMBERS_AMOUNT;
        line += 1;
    }
}

fn analyze<'a>(buffer: impl Iterator<Item = &'a u128>, parsed_number: u128) -> Option<u128> {
    let processing_buffer: ArrayVec<_, SAFE_NUMBERS_AMOUNT> = buffer
        .filter(|buffered_number| parsed_number > **buffered_number)
        .collect();

    for (numbers_traversed, buffered_number) in processing_buffer.iter().enumerate() {
        let target_number = parsed_number - *buffered_number;

        if processing_buffer[numbers_traversed + 1..]
            .binary_search(&&target_number)
            .is_ok()
        {
            return None;
        }
    }

    Some(parsed_number)
}

fn parser(raw_number: &mut String, bytes_read: usize) -> Result<u128, Error> {
    if let Ok(parsed_number) = raw_number[..bytes_read - 1].parse() {
        raw_number.clear();

        Ok(parsed_number)
    } else {
        Err(ErrorKind::InvalidData.into())
    }
}
